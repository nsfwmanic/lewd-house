
screen explore():
    tag explore
    use MyRoom
##      Room Screens
screen MyRoom():
    tag explore
    add "MyRoomExit.jpg"
    imagebutton auto "MyRoomDoor_%s.png" pos 374, 0 action Show ("MyRoomDoor")
    imagebutton auto "MyRoomBed_%s.png" pos 0, 156 action Show("Bed")
    imagebutton auto "MyRoomDesk_%s.png" pos 870, 0 action Show("Desk")
screen Bed():
    tag explore
    add "MyRoom.jpg"
    imagebutton auto "BedMatress_%s.png" pos 188, 266 action Show("BedTop")
    imagebutton auto "BedUnder_%s.png" pos 181, 570 action Jump("UnderBed")
    imagebutton auto "BedDoor_%s.png" pos 1088, 0 action Show("MyRoomDoor")
screen BedTop(nnext = 'day'):
    tag explore
    add "MyRoom.jpg"
    if act == 'night':
        imagebutton auto "BedButtonWait_%s.png" pos 210, 245 action SetVariable('act', 'morning'),Jump("BedMorning")
    if act == 'morning':
        imagebutton auto "BedButtonWait_%s.png" pos 210, 245 action SetVariable('act', 'afternoon'),Jump("BedWait")
    if act == 'afternoon':
        imagebutton auto "BedButtonWait_%s.png" pos 210, 245 action SetVariable('act', 'evening'),Jump("BedWait")
    if act == 'evening':
        imagebutton auto "BedButtonWait_%s.png" pos 210, 245 action SetVariable('act', 'night'),Jump("BedNight")
    imagebutton auto "BedButtonNight_%s.png" pos 410, 245 action  SetVariable('act', 'night'), Jump("BedNight")
    imagebutton auto "BedButtonSleep_%s.png" pos 610, 245 action SetVariable('act', 'morning'),Jump("BedMorning")
    #imagebutton auto "BedButtonNap_%s.png" pos 810, 245 action Jump("BedNap")
    imagebutton auto "ButtonBack_%s.png" pos 530, 430 action Jump("MyRoom")
screen Desk():
    tag explore
    add "DeskA.png"
    imagebutton auto "deskMirror_%s.png" pos 367, 0 action Jump("Mirror")
    imagebutton auto "desklaptop_%s.png" pos 496, 289 action Jump("Laptop")
    imagebutton auto "deskLeftDrawer_%s.png" pos 301, 555 action Jump("LeftDrawer")
    imagebutton auto "deskRightDrawers_%s.png" pos 682, 554 action Jump("RightDrawers")
    imagebutton auto "deskDoor_%s.png" pos 0, 0 action Show("MyRoomDoor")
screen MyRoomDoor():
    tag explore
    add "MyRoomExit.jpg"
    imagebutton auto "MyRoomCalendarButton_%s.png" pos 540, 60 action Call("Calendar")
    imagebutton auto "MyRoomLeaveButton_%s.png" pos 540, 240 action Jump("MyRoomLeave")
    imagebutton auto "MyRoomStayButton_%s.png" pos 540, 420 action Show("MyRoom")
####|---------------------------------------------|
## Hallway and rooms
screen Hall():
    tag explore
    add "Hallway.png"
    imagebutton auto "images/Hallway/LilyLisaRoom_%s.png" pos 98, 8 action Jump("LilyLisaRoom")
    imagebutton auto "images/Hallway/Stairs_%s.png" pos 365, 99 action Show("Stairs")
    imagebutton auto "images/Hallway/LeniLoriRoom_%s.png" pos 497, 255 action Show("LeniLoriRoom")
    imagebutton auto "images/Hallway/BathroomDoor_%s.png" pos 585, 297 action Show("Bathroom")
    imagebutton auto "images/Hallway/LuanLunaRoom_%s.png" pos 694, 255 action Show("LuanLunaRoom")
    imagebutton auto "images/Hallway/LucyLynnRoom_%s.png" pos 770, 161 action Show("LucyLynnRoom")
    imagebutton auto "images/Hallway/LanaLolaRoom_%s.png" pos 1004, 0 action Show("LanaLolaRoom")
    #imagebutton auto "images/Hallway/Attic_%s.png" pos 545, 154 action Jump("Attic")
    imagebutton auto "ButtonBack_%s.png" pos 560, 560 action Show("MyRoom")
screen Rooms():
    imagebutton auto "ButtonBack_%s.png" pos 550, 600 action Show("Hall")

screen LeniLoriRoom():
    tag explore
    add "images/LeniLoriRoom/LeniLoriRoom.jpg"
    imagebutton auto "images/LeniLoriRoom/LeniLoriDrawer_%s.png" pos 79, 288 action Jump("LeniLoriMirror")
    imagebutton auto "images/LeniLoriRoom/LeniLoriCloset_%s.png" pos 286, 223 action Jump("LeniLoriCloset")
    imagebutton auto "images/LeniLoriRoom/LoriBed_%s.png" pos 771, 387 action Show("LoriSide")
    imagebutton auto "images/LeniLoriRoom/LeniBed_%s.png" pos 896, 508 action Show("LeniSide")
    imagebutton auto "images/LeniLoriRoom/LeniLoriVent_%s.png" pos 246, 106 action Jump("LeniLoriVent")
    imagebutton auto "images/LeniLoriRoom/LeniLoriSewingMachine_%s.png" pos 378, 310 action Jump("SewingMachine")
    use Rooms
screen LuanLunaRoom():
    tag explore
    add "images/LuanLunaRoom/LuanLunaRoom.jpg"
    if MapShow == True:
        imagebutton auto "images/LuanLunaRoom/minimapLuanLuna_%s.png" pos 310, 20
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaLeft_%s.png" pos 310, 77 action Show("LuanLunaLeft")
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaRight_%s.png" pos 387, 77 action Show("LuanLunaRight")
    use Rooms
screen LucyLynnRoom():
    tag explore
    add "images/LucyLynnRoom/LucyLynnRoom.jpg"
    imagebutton auto "images/LucyLynnRoom/LucyBedButton_%s.png" pos 0, -4 action Show("LucyBed")
    imagebutton auto "images/LucyLynnRoom/LynnBedButton_%s.png" pos 752, 280 action Show("LynnBed")
    if MapShow == True:
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynn_%s.png" pos 310, 20
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnDesk_%s.png" pos 310, 102 action Show("LucyDesk")
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnCloset_%s.png" pos 366, 102 action Show("LucyLynnCloset")
    use Rooms
screen LanaLolaRoom():
    tag explore
    add "images/LanaLolaRoom/LanaLolaRoom.jpg"
    imagebutton auto "images/LanaLolaRoom/LanaBedButton_%s.png" pos 0, 101 action Show("LanaBed")
    imagebutton auto "images/LanaLolaRoom/LolaBedButton_%s.png" pos 730, 0 action Show("LolaBed")
    if MapShow == True:
        imagebutton auto "images/LanaLolaRoom/minimapLolaLana_%s.png" pos 310, 20
        imagebutton auto "images/LanaLolaRoom/minimapLanaLolaEntrance_%s.png" pos 310, 91 action Jump ("ExitHall")
        imagebutton auto "images/LanaLolaRoom/minimapLolaTea_%s.png" pos 380, 91 action Show("LolaTea")
    use Rooms

screen Bathroom():
    tag explore
    add "images/Bathroom/Bathroom.jpg"
    imagebutton auto "images/Bathroom/BathroomVent_%s.png" pos 59, 554 action Jump("BathroomVent")
    imagebutton auto "images/Bathroom/BathroomSink_%s.png" pos 99, 0 action Jump ("BathroomSink")
    imagebutton auto "images/Bathroom/Shower_%s.png" pos 428, 52 action Jump ("BathTub")
    imagebutton auto "images/Bathroom/Toilet_%s.png" pos 887, 179 action Jump ("Toilet")
    imagebutton auto "images/Bathroom/BathroomExit_%s.png" pos 1132, 0 action Show("Hall")
    use Rooms
####|---------------------------------------------|
#### Down Stairs
screen Stairs():
    tag explore
    add "Stairs.png"
    imagebutton auto "StairsButtonLiving_%s.png" pos 530 , 90 action Show("LivingRoom")
    imagebutton auto "StairsButtonDining_%s.png" pos 530 , 310 action Show("DiningRoom")
    imagebutton auto "StairsButtonKitchen_%s.png" pos 530 , 530 action Show("Kitchen")
    use Rooms
screen DiningRoom():
    tag explore
    add "images/DiningRoom/DiningRoomEnter.png"
    if MapShow == True:
        imagebutton auto "images/DiningRoom/minimapDining_%s.png" pos 310, 20
        imagebutton auto "images/DiningRoom/minimapDiningExits_%s.png" pos 310, 20 action Show("DiningExits")
    imagebutton auto "ButtonBack_%s.png" pos 560, 560 action Show("DiningExits")
screen DiningExits():
    tag explore
    add "images/DiningRoom/DiningRoomExits.png"
    imagebutton auto "images/DiningRoom/DiningKitchenExit_%s.png" pos 693, 0 action Show("Kitchen")
    imagebutton auto "images/DiningRoom/DiningLivingExit_%s.png" pos 0, 106 action Show("LivingRoom")
    if MapShow == True:
        imagebutton auto "images/DiningRoom/minimapDining_%s.png" pos 310, 20
        imagebutton auto "images/DiningRoom/minimapDiningFront_%s.png" pos 386, 20 action Show("DiningRoom")
screen Kitchen():
    tag explore
    add "images/Kitchen/Kitchen.jpg"
    if MapShow == True:
        imagebutton auto "images/Kitchen/minimapKitchen_%s.png" pos 310, 20
        imagebutton auto "images/Kitchen/minimapKitchenBackExits_%s.png" pos 310, 20 action Show("Backdoor")
        imagebutton auto "images/Kitchen/minimapKitchenDiningExit_%s.png" pos 310, 93 action Jump("KitchenDiningExit")
    imagebutton auto "ButtonBack_%s.png" pos 560, 560 action Show("DiningExits")
screen Backdoor():
    tag explore
    add "images/Kitchen/KitchenDoorways.jpg"
    if MapShow == True:
        imagebutton auto "images/Kitchen/minimapKitchen_%s.png" pos 310, 20
        imagebutton auto "images/Kitchen/minimapKitchenDiningExit_%s.png" pos 310, 93 action Jump("KitchenDiningExit")
        imagebutton auto "images/Kitchen/minimapKitchenFront_%s.png" pos 388, 20 action Show("Kitchen")
    imagebutton auto "images/Kitchen/BasementDoor_%s.png" pos 83, 0 action Jump("Basement")
    imagebutton auto "images/Kitchen/BackyardDoorExit_%s.png" pos 1195, 0 action Jump("Backyard")
screen LivingRoom():
    tag explore
    add "images/LivingRoom/LivingRoomView.png"
    imagebutton auto "images/LivingRoom/LivingRoomStairs_%s.png" pos 458, 0 action Jump("Upstairs")
    imagebutton auto "images/LivingRoom/LivingDiningEntrance_%s.png" pos 1088, 0 action Show("DiningRoom")
    #imagebutton auto "images/LivingRoom/MasterBedroomEntrance_%s.png" pos 0, 0 action Jump("Parentsroom")
    if MapShow == True:
        imagebutton auto "images/LivingRoom/minimapLivingRoom_%s.png" pos 310, 20
        imagebutton auto "images/LivingRoom/minimapLivingRoomFireplace_%s.png" pos 310, 45 action Jump("Fireplace")
        imagebutton auto "images/LivingRoom/minimapLivingRoomTV_%s.png" pos 346, 129 action Jump("TV")
        imagebutton auto "images/LivingRoom/minimapLivingRoomExit_%s.png" pos 459, 102 action Jump("LivingRoomExit")
####|---------------------------------------------|
#### Lori Leni Room
screen LoriSide():
    tag explore
    add "images/LeniLoriRoom/LoriBed.png"
    imagebutton auto "ButtonBack_%s.png" pos 550, 600 action Show("LeniLoriRoom")
screen LeniSide():
    tag explore
    add "images/LeniLoriRoom/LeniBed.png"
    imagebutton auto "ButtonBack_%s.png" pos 550, 600 action Show ("LeniLoriRoom")
####|---------------------------------------------|
#### Luan Luna
screen LuanLunaRight():
    tag explore
    add "images/LuanLunaRoom/LuanWall.jpg"
    if MapShow == True:
        imagebutton auto "images/LuanLunaRoom/minimapLuanLuna_%s.png" pos 310, 20
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaLeft_%s.png" pos 310, 77 action Show("LuanLunaLeft")
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaDefault_%s.png" pos 310, 20 action Show("LuanLunaRoom")
    imagebutton auto "images/LuanLunaRoom/LuanLunaRightExit_%s.png" pos 967, 26 action Show ("Hall")
screen LuanLunaLeft():
    tag explore
    add "images/LuanLunaRoom/LuanLunaLeftSide.png"
    imagebutton auto "images/LuanLunaRoom/LuanLunaLeftExit_%s.png" pos 57, 0 action Show ("Hall")
    imagebutton auto "images/LuanLunaRoom/LuanLunaCloset_%s.png" pos 329, 144 action Jump("LuanLunaCloset")
    imagebutton auto "images/LuanLunaRoom/LunaDrums_%s.png" pos 264, 334 action Jump("LunaDrums")
    imagebutton auto "images/LuanLunaRoom/LuanLunaLeftBeds_%s.png" pos 776, 119 action Show("LuanLunaBeds")
    imagebutton auto "images/LuanLunaRoom/LuanLunaLadder_%s.png" pos 933, 173 action Jump("LunaBed")
    if MapShow == True:
        imagebutton auto "images/LuanLunaRoom/minimapLuanLuna_%s.png" pos 310, 20
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaRight_%s.png" pos 387, 77 action Show("LuanLunaRight")
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaDefault_%s.png" pos 310, 20 action Show("LuanLunaRoom")
screen LuanLunaBeds():
    tag explore
    add "images/LuanLunaRoom/LuanLunaBeds.png"
    if MapShow == True:
        imagebutton auto "images/LuanLunaRoom/minimapLuanLuna_%s.png" pos 310, 20
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaRight_%s.png" pos 387, 77 action Show("LuanLunaRight")
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaLeft_%s.png" pos 310, 77 action Show("LuanLunaLeft")
        imagebutton auto "images/LuanLunaRoom/minimapLuanLunaDefault_%s.png" pos 310, 20 action Show("LuanLunaRoom")
####|---------------------------------------------|
#### Lucy Lynn
screen LucyDesk():
    tag explore
    add "images/LucyLynnRoom/LucyDesk.jpg"
    if MapShow == True:
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynn_%s.png" pos 310, 20
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnBeds_%s.png" pos 310, 20 action Jump("LucyLynnRoom")
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnCloset_%s.png" pos 366, 102 action Show ("LucyLynnCloset")
    imagebutton auto "images/LucyLynnRoom/LucyDeskDoor_%s.png" pos 0, 0 action Show ("Hall")
screen LucyLynnCloset():
    tag explore
    add "images/LucyLynnRoom/LucyLynnCloset.jpg"
    imagebutton auto "images/LucyLynnRoom/LucyLynnCloset_%s.png" pos 62, 27 action Jump("LucyLynnCloset")
    imagebutton auto "images/LucyLynnRoom/LynnDoorExit_%s.png" pos 507, 34 action Show ("Hall")
    if MapShow == True:
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynn_%s.png" pos 310, 20
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnBeds_%s.png" pos 310, 20 action Jump("LucyLynnRoom")
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnDesk_%s.png" pos 310, 102 action Show ("LucyDesk")
screen LucyBed():
    tag explore
    add "images/LucyLynnRoom/LucyBed.jpg"
    if MapShow == True:
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynn_%s.png" pos 310, 20
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnBeds_%s.png" pos 310, 20 action Jump("LucyLynnRoom")
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnDesk_%s.png" pos 310, 102 action Show("LucyDesk")
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnCloset_%s.png" pos 366, 102 action Show("LucyLynnCloset")
screen LynnBed():
    tag explore
    add "images/LucyLynnRoom/LynnBed.jpg"
    if MapShow == True:
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynn_%s.png" pos 310, 20
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnBeds_%s.png" pos 310, 20 action Jump("LucyLynnRoom")
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnDesk_%s.png" pos 310, 102 action Show("LucyDesk")
        imagebutton auto "images/LucyLynnRoom/minimapLucyLynnCloset_%s.png" pos 366, 102 action Show("LucyLynnCloset")
####|---------------------------------------------|
#### Lana Lola
screen LanaBed():
    tag explore
    add "images/LanaLolaRoom/LanaBed.jpg"
    if MapShow == True:
        imagebutton auto "images/LanaLolaRoom/minimapLolaLana_%s.png" pos 310, 20
        imagebutton auto "images/LanaLolaRoom/minimapLolaLanaDefault_%s.png" pos 310, 20 action Show("LanaLolaRoom")
        imagebutton auto "images/LanaLolaRoom/minimapLanaLolaEntrance_%s.png" pos 310, 91 action Jump("ExitHall")
        imagebutton auto "images/LanaLolaRoom/minimapLolaTea_%s.png" pos 380, 91 action Show("LolaTea")
screen LolaBed():
    tag explore
    add "images/LanaLolaRoom/LolaBed.png"
    if MapShow == True:
        imagebutton auto "images/LanaLolaRoom/minimapLolaLana_%s.png" pos 310, 20
        imagebutton auto "images/LanaLolaRoom/minimapLolaLanaDefault_%s.png" pos 310, 20 action Show("LanaLolaRoom")
        imagebutton auto "images/LanaLolaRoom/minimapLanaLolaEntrance_%s.png" pos 310, 91 action Jump("ExitHall")
        imagebutton auto "images/LanaLolaRoom/minimapLolaTea_%s.png" pos 380, 91 action Show("LolaTea")
screen LolaTea():
    tag explore
    add "images/LanaLolaRoom/LolaTea.png"
    imagebutton auto "images/LanaLolaRoom/kitchenplaysetdoor_%s.png" pos 941, 0 action Jump("ExitHall")
    imagebutton auto "images/LanaLolaRoom/kitchenplaysetoven_%s.png" pos 481, 282 action Jump("LolaKitchenSet")
    imagebutton auto "images/LanaLolaRoom/LolaTable_%s.png" pos 15, 437 action Jump("LolaTableSet")
    if MapShow == True:
        imagebutton auto "images/LanaLolaRoom/minimapLolaLana_%s.png" pos 310, 20
        imagebutton auto "images/LanaLolaRoom/minimapLolaLanaDefault_%s.png" pos 310, 20 action Show("LanaLolaRoom")
        imagebutton auto "images/LanaLolaRoom/minimapLanaLolaEntrance_%s.png" pos 310, 91 action Jump("ExitHall")
screen LolaSide():
    tag explore
    imagemap:
        ground "LolaCloset.jpg"
        hover "LolaClosetb.jpg"

        hotspot (939, 0, 303, 695) clicked Jump("LolaCloset")
####|---------------------------------------------|
screen main_menu():

    # This ensures that any other menu screen is replaced.
    tag menu

    style_prefix "main_menu"

    add gui.main_menu_background

    # This empty frame darkens the main menu.
    frame:
        pass

    ## The use statement includes another screen inside this one. The actual
    ## contents of the main menu are in the navigation screen.
    use navigation
screen navigation():

    vbox:
        style_prefix "navigation"
        xpos 60
        yalign 0.5

        spacing gui.navigation_spacing

        if main_menu:

            textbutton _("Start") action Start()
            textbutton _("Tutorial") action Start('Prologue')

        else:

            textbutton _("History") action ShowMenu("history")

            textbutton _("Save") action ShowMenu("save")

        textbutton _("Load") action ShowMenu("load")

        textbutton _("Options") action ShowMenu("preferences")

        if _in_replay:

            textbutton _("End Replay") action EndReplay(confirm=True)

        elif not main_menu:

            textbutton _("Main Menu") action MainMenu()

        textbutton _("About") action ShowMenu("about")

        if renpy.variant("pc"):

            ## Help isn't necessary or relevant to mobile devices.
            #textbutton _("Help") action ShowMenu("help")

            ## The quit button is banned on iOS and unnecessary on Android.
            textbutton _("Quit") action Quit(confirm=not main_menu)

define convo_options = ["Dogs"]
screen screen_convo():
    #add girl
    vbox:
        align .95, 0.5
        textbutton 'Talk' action [SetVariable("act", "talk"), Call("events_run_period")]
        textbutton 'Flirt' action [SetVariable("act", "flirt"), Call("events_run_period")]
        textbutton 'Flirt2' action Function( Act, "night" )
        for x in convo_options:
            textbutton x action [SetVariable("act", "x"), Call("events_run_period")]
        textbutton 'Leave' action Return()
        pass
