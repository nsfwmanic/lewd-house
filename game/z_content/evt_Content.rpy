label wip:
    "Work in Progress"
    return
label end:
    $renpy.full_restart()
label end_check:
    if date >= enddate:
        "Game Over"
        $ renpy.full_restart
    return
label empty:
    return
label intro:
    #   Insert that intro here
    #Start with a shot of the house. Lincoln has a voice over moment.
    scene bg HouseNight

    linc "Tomorrow is the first day of summer. Which is exciting, but its even more exciting when you have 10 sisters."

    #Its late. All the sisters are on the couch watching a show. At this point you can click on any of them. And a leave button.
    #Clicking on them gives you a quick description of that sister.

    #When you click leave.

    "The show you and your sister's were watching is over."

    "The sisters go to their rooms and its like a stampede."

    "goo goo"
    lisa "What our verbally challeged little sister means is Good night, lincoln. Same for me"
    lola " Night lincoln, now for my beauty sleep."
    lana "Good night lincoln, be careful with the bedbugs. I really mean it mine have been missing for a while."
    lucy "Id say good night. But you know I become nocturnal once we are out of school."
    lynn "Don't stay up too late Linc, you'll miss the pre-dawn run."
    #(Yeah, like I have any intention of getting up before 5AM this summer)
    luna "I'm up all night till the sun, I'm up all night to get some, but don't copy everything you see bro."
    luan "It's sock it to me time! Because the sandbag keeps punching me in the face! Get it? Ha ha ha ha ha....but seriously if I don't get some sleep it'll be no laughing matter."
    #show lori banister(temp name)
    "Lori stops with her hand on the banister."
    lori  "Its summertime, Lincoln, but don't stay up all night. You don't want to sleep your summer away."
    lori "And if you want me yo give you a lift it will have to be in the mornings"
    lori "I'm working full time at the arcade this summer so I can afford to take time off when I'm in college."
    lori "And I can't ask Bobby to give me a job at the bodega, the Casagrandes really need the income."
    leni "Do you like her shorts too Linky?"
    leni "Your staring at them."
    leni "I like, made them for her after she ripped her last ones. I think her butt is getting bigger."
    leni "I like can't even fit them anymore. They like slide right off me now."
    return


label Breakfast:
    return
label Lunch:
    return
label Dinner:
    return
#--------------------------------------------#
#--------------------------------------------#

label MyRoom:
    call show_screens from _call_show_screens_9
    return

label UnderBed:
    scene bg Underbed
    "For now this will only be a place you can find Lucy in, during certain times."
    return

label BedWait:
    scene bg BedEnter
    "You decide to rest for a little bit."
    if DayPhase <= 9:
        $ DayPhase += 1
    if DayPhase == 2:
        jump Breakfast
    if DayPhase == 5:
        jump Lunch
    if DayPhase == 8:
        jump Dinner
    if DayPhase == 9:
        "I should just go to bed"
        call screen Bed
    call screen MyRoom
label BedNap:
    scene bg BedEnter
    "You decide to nap for a bit."
    if DayPhase <= 5:
        $ DayPhase = 5
        jump Lunch
    elif DayPhase >= 5 and DayPhase <= 8:
        $ DayPhase = 8
        jump Dinner
    elif DayPhase >= 8:
        jump NightPhase
label BedNight:
    scene bg BedSleep
    "You decide to rest until nightfall."
    scene bg HouseLightsOff with Fade(0.5, 1.0, 0.2)
    $ DayPhase = 10
    "It is now night."
    scene bg MyRoomNight
    jump NightPhase
label BedMorning:
    scene bg BedEnter
    if DayPhase <= 9 or DayPhase <= 10:
        "It's too early."
        jump MyRoom
    else:
        scene bg BedSleep
        "You decide to rest until morning."
        scene bg Morning with Fade(0.5, 1.0, 0.2)
        "It is now morning."
        $ DayPhase = 1
        if Sunday == True:
            $ Sunday = False
            $ Monday = True
        elif Monday == True:
            $ Monday = False
            $ Tuesday = True
        elif Tuesday == True:
            $ Tuesday = False
            $ Wednesday = True
        elif Wednesday == True:
            $ Wednesday = False
            $ Thursday = True
        elif Thursday == True:
            $ Thursday = False
            $ Friday = True
        elif Friday == True:
            $ Friday = False
            $ Saturday = True
        elif Saturday == True:
            $ Saturday = False
            $ Sunday = True
            scene bg Morning
            return

label Mirror:
    scene bg MyMirror
    "You can use this mirror to check your stats once stats are finalized."
    call screen MyRoom
label Laptop:
    scene bg Laptop
    "You can use this to search the Internet."
    "Dev Note:The purpose of this option to research specific topics that will augment your abilities
    and allow you to better handle sisters."
    "Also you will be able to shop for items using this terminal."
    scene bg Laptopb
    "Here is an example screen of the laptop in action."
    call screen MyRoom
label LeftDrawer:
    scene bg LeftDrawer
    "Can store items here."
    call screen MyRoom
label RightDrawers:
    scene bg RightDrawers
    "Can store items here."
    call screen MyRoom
label Calendar:
    scene bg Calendar
    "No events for this month apparently."
    "Dev Note: I plan for the game to have both a time system and a day/night cycle."
    "Actions and events take a certain amount of time to complete."
    "You can only do so much before night falls."
    "This will have players decide which events they want to do and which sisters they want to focus on."
    "Some events like creeping around are better done at night."
    "Player would use the calendar to keep track of special events that happen on a specific day."
    call screen MyRoom
label MyRoomLeave:
    scene bg Hallway
    linc "Where should I head to next?"
    call screen Hall
label MyRoomStay:
    scene bg MyRoomExit
    call screen MyRoom

label LilyLisaRoom:
    scene bg LilyLisaRoom

#    label Downstairs:
#        scene bg LivingRoomView
##            menu:
#              "Where do I want to go?"
#               "The Living Room.":
#                   scene bg LivingRoomCenter
#                   "Continuing on, you head toward the front door."
#                   jump LivingRoom
#
#               "The Dining Room.":
#                   scene bg FirstFloorMain
#                   "Making your way to the left, you head into the dining room."
#                   jump DiningRoom
#
#               "Outside.":
#                   scene bg FrontDoorInside
#                   "Continuing on, you head toward the front door."
#                   scene bg FrontPorch
#                   if Tuesday == True and DayPhase == 4:
#                       jump LuanTuesdayMorning3
#                   elif Wednesday == True and DayPhase == 3:
#                       jump LanaWednesdayMorning2
#                   elif Thursday == True and DayPhase == 4:
#                       jump LanaThursdayMorning3
#                   elif Friday == True and DayPhase == 4:
#                       jump LunaFridayMorning3
#                   elif Friday == True and DayPhase == 3:
#                      jump LynnFridayMorning2
#                  elif Saturday == True and DayPhase == 3:
#                      jump LoriSaturdayMorning2
#                  else:
#                      "You are now on the front porch. Let's stay inside the house for now."
#                     jump Downstairs
#              "Back up the stairs.":
#                 scene bg StairsGoingUp
#                 "You decide to turn around and head back upstairs."
#                 call screen Hall
#                "Look out the window":
#                    scene bg DiningRoomWindow
#                    "You turn your gaze toward the dining room window."
#                    "No one seems to be outside in the backyard right now."
#                    jump DiningRoom
#                "Check the trophy case":
#                  "This is a trophy case full of your sisters' accomplishments."
#                    "Dev Note: You will be able to stash things here once items are added."
#                    "Perhaps I could use this area to bring up an achievement screen?"
#                    "Anyway, for now its just another backdrop to hold conversations in front of."
#                    jump DiningRoom
#                "Check under the table":
#                    scene bg UnderTable
#                   "This is an additional hiding place along with the one in your own room and under Lori's bed."
#                   "In a later build in the far future, you will able to install hidden cameras around the house to record the sisters and creep on them."
#                   jump DiningRoom
#
#               "Go into the Kitchen":
#                   stop music fadeout 1.0
#                   "You decide to make your way to the Kitchen."
#                   jump Kitchen
#               "Back to the stairs.":
#                   "Time to head back."
#                   call screen Hall
label KitchenDiningExit:
    scene bg KitchenDiningExit
    "Let's go to the dining room."
    call screen DiningRoom
#            "Check the fridge.":
#                    scene bg KitchenFridge
#                    if Monday == True and DayPahse == 3:
#                        Luna "Hey bro, what do you need?"
#                        Luna "I can hook you up."
#                    else:
#                        "Its just a standard kitchen appliance."
#                        jump Kitchen
#            "Head to rear entrance.":
#                scene bg KitchenDoorways
#                "From the kitchen, you walk toward the entryway that between the basement and backyard."
#                jump RearEntrance
#            "Head to Dining Room.":
#                scene bg DiningRoomEntrance
#                "You decide to make your way to the dining room."
#                jump DiningRoom
#
#            "Back to the stairs.":
#                "Time to head back."
#                call screen Hall

##  Return issues
label RearEntrance:
    scene bg KitchenDoorways
    menu:
        "Where do you want to go?"
        "Head down the basement.":
            jump Basement

        "Go outside to the backyard.":
            jump Backyard

        "Back into the Kitchen":
            stop music fadeout 1.0
            "You decide to make your way back to the Kitchen."
            return
            #jump Kitchen
label Basement:
    scene bg BasementSteps
    "You are in the basement."
    menu:
        "What do you want to do?"
        "Check out the boiler.":
            scene bg BasementBoiler
            "You move further down the basement and arrive at the boiler."
            "There is nothing here at the moment."
            jump Basement

        "Check out the circuit breaker.":
            scene bg CircuitBreaker
            "This is the circuit breaker."
            "This panel controls the electricity of the house."
            "Switching this off will cause a blackout."
            "Better not mess with it."
            "Dev Note:No real plans for how to use this as a feature yet. Since I need dark variants for all the rooms of the house for this to work."
            jump Basement

        "Check out the Laundry machines.":
            scene bg LaundryMachines
            if Friday == True and DayPhase == 3:
                jump LanaFridayMorning2
            else:
                "Its not in use right now."
                "You should probably come back here when you need to do laundry or retrieve some clothes."
                "Dev Note:This might function as a quest location later down the road. For now its a low quality backdrop. Sorry."
                jump Basement

        "Examine laundry basket.":
            scene bg LaundryPile
            "The basket has some clothes in it."
            "Dev Note:This is one of the baskets that can be found in the game."
            "The other can be found upstairs in the main hallway."
            "In later builds you will be able to find lingerie in these baskets to keep/use in your inventory."
            "But for this build, its just here to look at."
            jump Basement

        "Leave the basement.":
            if NightTime == True:
                jump KitchenNight
            else:
                "You decide to head back to the rear entrance."
                stop music fadeout 1.0
                call screen Kitchen
label Backyard:
    scene bg Backyard
    menu:
        "Talk to...":
            menu:
                "Who do you want to talk to?"

                "Lynn" if Monday == True and DayPhase == 4:
                    jump LynnMondayMorning3
                "Lynn" if Monday == True and DayPhase == 7:
                    jump LynnMondayAfternoon2
                "Lana" if Monday == True and DayPhase == 4:
                    jump LanaMondayMorning3
                "Lana" if Tuesday == True and DayPhase == 3:
                    jump LanaTuesdayMorning2
                "Lana" if Tuesday == True and DayPhase == 4:
                    jump LanaTuesdayMorning3
                "Lynn" if Tuesday == True and DayPhase == 7:
                    jump LynnTuesdayAfternoon2
                "Luna" if Wednesday == True and DayPhase == 4:
                    jump LunaWednesdayMorning3
                "Lynn" if Wednesday == True and DayPhase == 3:
                    jump LynnWednesdayMorning2
                "Return":
                    jump Backyard
        "Go back inside":
            call screen Kitchen

#                "Go to the couch.":
#                    scene bg LivingRoomCouch
#                    "You head over to the couch."
#                    "It looks like no one wants to watch TV at the moment. How weird."
#                    "Dev Note:A lot of interaction with the sisters will happen here in this room."
#                   "You will be able to invite sisters to hang out here with you in later builds."
#                   "Sisters will argue about what to watch."
#                   "You might have to pick a side and play favorites in situations like these."
#                   jump LivingRoom
##               "Go to the armchair.":
#                  scene bg LivingRoomArmchair
#                  "You head to the big armchair. Lucky for you, no one is here at the moment. You have the chair all to yourself."
#                  jump LivingRoom

label Upstairs:
    scene bg StairsGoingUp
    call screen Hall
label TV:
    scene bg TV
    "You sit down on the couch, grab the remote, and turn the TV on."
    "Its a broadcast of your favorite band, Smooch."
    "You watch it for a little bit."
    "After a few minutes, You decide to stop watching since you have other things to do around the house."
    call screen LivingRoom
label Fireplace:
    scene bg Fireplace
    "You walk to the fireplace."
    "Its not in use right now."
    "Dev Note: I don't know if I have any ideas on what special interactions could take place here."
    "I think it will just be another part of the house where you could hide items and talk to sisters."
    call screen LivingRoom
label LivingRoomExit:
    scene bg FrontDoorDining
    "Work in Progress"
    call screen LivingRoom
label Parentsroom:
        scene bg ParentsRoomOpen
        "This is Mom and Dad's room."
        menu:
            "What do you want to do?"
            "Check the closet.":
                scene bg ParentsRoomClosetOpen
                "You open the closet."
                scene bg ParentsClosetInside
                "Its full of your Dad's ties."
                "Time to leave."
                return
                #jump Downstairs

            "Leave.":
                scene bg ParentsRoomOpen
                "That is probably for the best."
                return
                #jump Downstairs
label LeniLoriRoom:
        scene bg LeniLoriRoom
        "This is Leni and Lori's room."
        call screen LeniLoriRoom
label LeniLoriRoomExit:
    scene bg LeniLoriRoomExit
    "Let's leave"
    call screen Hall
label LeniLoriMirror:
    scene bg LeniLoriMirror
    "Work in Progress"
    jump LeniLoriRoom
label LeniLoriCloset:
    scene bg LeniLoriCloset
    "You open the shared closet of Lori and Leni."
    jump LeniLoriRoom
label SewingMachine:
    scene bg SewingMachine
    "This is mostly used by Leni."
    "You better not mess with it without asking for Leni's permission."
    "Dev Note: The plan is for you to be able to buy materials that you could give to Leni to make clothes which are used to make costumes."
    "Leni is like super into fashion so showing an interest in her passion will boost your standing with her even faster."
    jump LeniLoriRoom
label LeniLoriVent:
    scene bg LoriVentClosed
    "You glance up to the ceiling. Its too high for you to reach. It seems to be one-way."
    jump LeniLoriRoom

#                        "Hide under Lori's bed.":
#                            scene bg LoriUnderBedA
#                            "At the sound of someone entering the room, you panic and hide under Lori's bed hoping you weren't seen."
#                            "Believing the coast clear, you slowly and noiselessly slip out from under the bed."
#                            menu:
#                                "Go further?"
#                                "Yes.":
#                                    scene bg LoriUnderBedB
#                                    "You crawl deeper underneath the bed and come across a pile of Lori's shoes."
#                                    "That's enough for now. Time to head back."
#                                    jump LeniLoriRoom
#
#                                "Forget it.":
#                                    "There is nothing worth getting from here anyway."
#                                    jump LeniLoriRoom
#
#

label Bathroomenter:
        call screen Bathroom
label BathroomSink:
        scene bg BathroomSink
        menu:
            "Do you want to use the sink?"
            "Sure.":
                play music Sink noloop
                "You wash your hands in the sink."
                "The cool water feels nice on your skin."
                "Dev Note: Once items are added, You will be interact with this sink more."
                "Perhaps tampering with the sisters' hygienic supplies?"
                call screen Bathroom

            "Not at the moment.":
                stop music fadeout 1.0
                call screen Bathroom
label BathTub:
        scene bg BathTub
        menu:
            "Do you want to take a shower?"
            "Sure.":
                "You decide to take a quick shower while the bathroom is empty."
                "Lucky for you, it seems your sisters are busy with stuff."
                "After rushing back to your room to get new clothes, you disrobe and get in the bathtub."
                scene bg ShowerInUse
                play music Shower noloop
                "The sensation of the hot water on your bare skin is soothing as it relaxes your muscles."
                "As the bathroom fills with steam, you savor this brief moment of peace."
                "Drying off with a towel, you hurry to get dressed to continue your day."
                stop music fadeout 1.0
                "Dev Note: Once the sisters are added you can choose this option to shower together if you meet the requirements."
                call screen Bathroom

            "Not really.":
                stop music fadeout 1.0
                call screen Bathroom
label Toilet:
        scene bg Toilet
menu:
    "What do you want to do?"
    "PEE PEE POO POO.":
        "You decide to use the toilet."
        play music Toilet noloop
        "You expel some Lincoln logs."
        stop music
        call screen Bathroom

    "Shoot my goo, my dude.":
        play music Fap noloop
        "You fap quickly and stealthily."
        stop music fadeout 1.0
        call screen Bathroom

    "Never mind.":
        stop music fadeout 1.0
        call screen Bathroom
label BathroomVent:
    stop music
    scene bg BathroomVent
    "You crouch down at the vent and try to listen for any sound."
    "Dev Note:An anon suggested using this area to eavesdrop on sisters while they are in their bedroom."
    "They could be talking about stuff like items they want that you could buy."
    "Or perhaps they might mention items you may have stolen from them without them knowing."
    menu:
        "Do you want to enter the vent?"
        "Yes.":
            scene bg VentsSideLight
            "You manage to open the grate and crawl through."
            scene bg VentsTurn
            "It seems that the path makes a turn here."
            scene bg VentsSide
            "Continuing on, you try to edge through the duct as soundlessly as possible."
            scene bg VentEnd
            "Peering through the slits, you can see into Leni and Lori's room."
            "Its time to head back. You return back to the bathroom."
            call screen Bathroom
        "No.":
            stop music fadeout 1.0
            call screen Bathroom
label BathroomExit:
        scene bg BathroomEntrance
        "You decide to leave the bathroom."
        stop music fadeout 1.0
        call screen Hall
label LuanLunaRoom:
        scene bg LuanLunaRoom
        "This is Luan and Luna's room."
        call screen LuanLunaRoom
label LuanLunaCloset:
    scene bg LuanLunaClosetInside
    "Work In Progress."
    call screen LuanLunaLeft
label LunaDrums:
    scene bg LuanLunaLeftSide
    "Work in Progress."
    call screen LuanLunaLeft
label LunaBed:
    scene bg LunaBed
    "Work in Progress."
    call screen LuanLunaLeft


#    label LunasDrums:
#        "Work In Progress"
#        call screen LuanLunaCloset
#    label LuanLunaClosetOpen:
#        scene bg LuanLunaClosetInside
#        "This sections need to be reworked to be uniform with the other sisters' rooms."
#        call screen LuanLunaCloset

label LucyLynnRoom:
        scene bg LucyLynnRoom
        "This is Lucy and Lynn's room."
        call screen LucyLynnRoom
label LucyLynnCloset:
    scene bg LucyLynnClosetOpen
    "Work In Progress"
    call screen LucyLynnCloset
label LanaLolaRoom:
        scene bg LanaLolaRoom
        "This is Lana and Lola's room."
        call screen LanaLolaRoom
label ExitHall:
    scene bg LanaLolaRoomExit
    "Let's leave."
    call screen Hall
label LanaLolaCloset:
    scene bg LolaClosetOpen
    "Work In Progress"
    call screen LolaBed
label LolaTableSet:
    scene bg LolaTea
    "Work in progress."
    call screen LolaTea
label LolaKitchenSet:
    scene bg LolaTea
    "There's nothing to do here right now."
    call screen LolaTea
#    screen LucyLynnCloset:
#          imagemap:
#                ground "LucyLynnCloset.jpg"
#                hover "LucyLynnClosetb.jpg"
#                hotspot (216, 79, 128, 588) clicked Jump("LucyLynnCloset")
#    label LolaCloset:
#            $ renpy.movie_cutscene("DoorOpenLolaCloset.mp4", delay = .6)
#            scene bg LolaClosetOpen
#            "You decide to open the closet."
#            "Luckily, Lola is not here; otherwise she would turn you into the 11th sister."
#            stop music fadeout 1.0
#            jump LanaLolaRoom
#    label LucyLynnCloset:
#            $ renpy.movie_cutscene("DoorOpenLucyLynnCloset.mp4", delay = 1.6)
#            scene bg LucyLynnClosetOpen
#            "You decide to open the closet."
#            "It doesn't seem like there is anything here that you can use at the moment."
#            stop music fadeout 1.0
#            jump LucyLynnRoom
label Attic:
    scene bg AtticEntrance
    "This is the entrance to the attic."
    menu:
        "Do you want to enter?"
        "Yes.":
            scene bg AtticFull
            "You are in the attic."

            if Saturday == True and DayPhase == 3:
                return
                #jump SaturdayAtticMovie
                "Luna is here"
            if Saturday == True and DayPhase == 4:
                jump LunaSaturdayMorning3
                "Lucy is here"
            if Thursday == True and DayPhase == 3:
                jump LucyThursdayMorning2
                "Lucy is here"
            if Thursday == True and DayPhase == 6:
                jump LucyThursdayAfternoon1
                "Lana is here"
            if Tuesday == True and DayPhase == 6:
                jump LanaTuesdayAfternoon1
            menu attic_menu:
                "What now?"

                "Rummage through the boxes.":
                    scene bg AtticBoxes
                    "You are spend a few moments searching through old boxes. Nothing of value here."
                    scene bg AtticFull
                    jump attic_menu

                "Explore by the lamp.":
                    scene bg AtticLamp
                    "You make your way to the corner of the attic containing an old lamp. The chest is locked."
                    "It doesn't seem like you can do anything more for now."
                    scene bg AtticFull
                    jump attic_menu

                "Check the area by the old TV set.":
                    scene bg AtticTV
                    "There are some things scattered around, but nothing really worth taking with you right now."
                    scene bg AtticFull
                    jump attic_menu

                "Time to leave.":
                    scene bg AtticExit
                    "You decide to head out and do other things around the house."
                    call screen Hall

        "Not right now.":
            "Let's go back to the main hall for now."
            call screen Hall
    return
#--------------------------------------------#
#--------------------------------------------#
label convo:
    show girlpic
    call screen screen_convo
    return
label talk:
    "Hi, Lincoln"
    return
label flirt:
    "Linconln you are so forward."
    return
label dogs:
    "How are you here?"
    "This is debug."
    "Ahggg something broke."
    "You shouldn't be seeing this."
    return
label leave:
    "Bye Lincoln"
    scene black with fade
    return

label start_convo:
    "Hi"
    return
label specialprompt:
    "What's wrong?"
    "Give item"
    "Repeat Event"
    return
label needitem:
    "I need [item]"
    return
label hasitem:
    "You have [item]. Thank you so much Lincoln."
    return
label afterEvent:
    "That was great, thanks to much"
    return
label temp_event:
    show placeholder
    "Stuff happened"
    hide placeholder
    return
